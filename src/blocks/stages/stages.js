$('.jsStageNext').click(function(e) {
  e.preventDefault();
  var activeIndex = $('.stagesItem_Active').index();
  $('.stagesItem').removeClass('stagesItem_Active');
  $('.stagesItem').eq(activeIndex+1).addClass('stagesItem_Active');
  $('.stagesHeaderItem').eq(activeIndex+1).addClass('stagesHeaderItem_Active');
})