$('.confirmation input').on("keyup change", function(e) {
  if ($(this).val) {
    $(this).next('input').focus();
  }
  
  var i = 0;
  $('.confirmation input').each(function() {
    if ($(this).val()) {
      i += 1; 
    }
  })
  
  if (i >= $('.confirmation input').length) {
    $('.confirmationNext').removeAttr('disabled');
  }
});


$('.confirmationNext').click(function(e) {
  e.preventDefault();
  $(this).addClass('btn-spinner');
})
