$('.integrations__btn').click(function(e) {
  e.preventDefault();
  $(this).hide();
  $('.integrations__item').fadeIn().css('display', 'flex');
});