$('.table__item--title').click(function(e) {
  e.preventDefault();
  $(this).closest('.table__row').find('.table__row-content').slideToggle();
});

$('.table__more').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('table__more--active');
  $(this).closest('.table__row').find('.table__row-content').slideToggle();
})
