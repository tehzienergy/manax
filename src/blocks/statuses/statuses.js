$('.statusesSectionItem').each(function() {
  var weight = $(this).text();
  var weightMax = $(this).closest('.statusesSection').find('.statusesSectionOverall').text();
  if (weight) {
    $(this).css('flex', '0 0 ' + weight/weightMax*100 + '%');
  }
});

function statusesPush() {
  var statusesOverall = [];

  $('.statusesSectionOverall').each(function() {
    statusesOverall.push($(this).text());
  });
  
  var minOverall = Math.min.apply(Math, statusesOverall).toString();
  
  var maxOverall = Math.max.apply(Math, statusesOverall).toString();

  var minOverallIndex = statusesOverall.indexOf(minOverall);

  var maxOverallIndex = statusesOverall.indexOf(maxOverall);
  
  var lgSectionHeight = $('.statusesSection').eq(maxOverallIndex).find('.statusesSectionColumn').height();
  
  var smSectionHeight = minOverall/maxOverall;

  $('.statusesSection').eq(minOverallIndex).find('.statusesSectionColumn').css('flex', '0 0 ' + smSectionHeight*100 + '%');
}

if ($('.statusesSection').length) {
  statusesPush();
}
