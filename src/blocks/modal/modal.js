Fancybox.bind('[data-fancybox]', {
  autoFocus: false,
  dragToClose: false
});

$('.modal__close').click(function(e) {
  e.preventDefault();
  Fancybox.close();
})
