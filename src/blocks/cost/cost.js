function costPrice() {
  var costArray = [];

  $('.costContent p').each(function() {
    var costPrice = $(this).text().substring(1).replace(/,/g, '');
    costArray.push(costPrice);
  });
  
  var maxCost = Math.max.apply(Math, costArray);
  
  var maxCostIndex = costArray.indexOf(maxCost.toString());
  
  var maxWidth = $('.cost').eq(maxCostIndex).find('.costColumn').width();
  
  $('.cost').each(function() {
    var i = $(this).index();
    var width = costArray[i]/maxCost*maxWidth;
    $(this).find('.costColumn').css('width', width + 'px').addClass('costColumn_Shorter');
  })
} 

if ($('.cost').length) {
  costPrice();
}
