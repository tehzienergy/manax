$('.notificationBtn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('notificationBtn_Less');
  $(this).closest('p').find('.notificationText').toggleClass('notificationText_Open')
});

$('.notification').each(function() {
  var text = $(this).find('.notificationText');
  var btn = $(this).find('.notificationBtn');

  if (text.prop('scrollWidth') > text.width() + 1 ) {
    btn.addClass('notificationBtn_Active');
  }
})