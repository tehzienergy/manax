$(document).ready(function () {
  $('.js-submit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var form_data = form.serialize();
    form.trigger('reset');
    $.ajax({
      type: 'POST',
      url: './send.php',
      data: form_data,
      success: function (data) {
        $('.form__content').hide();
        $('.form__success').fadeIn();
      }
    });
  });
});

$('.form__password-view').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('form__password-view--active');
  var inputType = $(this).closest('.form__item-password').find('.input');
  if (inputType.attr('type') == 'password') {
    inputType.attr('type', 'text');
  }
  else {
    inputType.attr('type', 'password');
  }
})
