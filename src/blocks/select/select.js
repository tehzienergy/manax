if ($('.select__input').length) {

  $('.select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      closeOnSelect: false,
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    });

    $(this).on('change', function () {
      let select = $(this)
      if ($(this).val().length > 2) {

        $(this).next('span.select2').find('ul').html(function () {
          let count = select.select2('data').length - 1;
          return "<li>Выбрано " + count + "</li>";
        })
      }
    });
  })

  $('.select_Countries .select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      templateResult: formatState,
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    }).on('select2:select', function (e) {
      var data = e.params.data;
      $('.select_Countries .select2-selection__rendered').prepend('<img src="./img/' + data.id + '.svg" class="select2-selection__rendered-icon">')
    });
    
    var countryInitial = $('.select_Countries .select__input').val();
    $('.select_Countries .select2-selection__rendered').prepend('<img src="./img/' + countryInitial + '.svg" class="select2-selection__rendered-icon">');
  });
  
  function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    var baseUrl = "./img";
    var $state = $(
      '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.svg" /> ' + state.text + '</span>'
    );
    return $state;
  };

  $('.select--normal .select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      placeholder: "Select",
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    })
  });
};
