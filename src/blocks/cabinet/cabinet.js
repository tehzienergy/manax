$('.cabinet__inner-section-more-btn').click(function(e) {
  e.preventDefault();
  $('.cabinet__inner-section-more-btn').toggleClass('cabinet__inner-section-more-btn--active');
  $('.cabinet__inner-section').find('.cabinet__inner-section-more').fadeToggle('fast');
})
