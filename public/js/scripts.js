









$('.cabinet__inner-section-more-btn').click(function(e) {
  e.preventDefault();
  $('.cabinet__inner-section-more-btn').toggleClass('cabinet__inner-section-more-btn--active');
  $('.cabinet__inner-section').find('.cabinet__inner-section-more').fadeToggle('fast');
})

$('.confirmation input').on("keyup change", function(e) {
  if ($(this).val) {
    $(this).next('input').focus();
  }
  
  var i = 0;
  $('.confirmation input').each(function() {
    if ($(this).val()) {
      i += 1; 
    }
  })
  
  if (i >= $('.confirmation input').length) {
    $('.confirmationNext').removeAttr('disabled');
  }
});


$('.confirmationNext').click(function(e) {
  e.preventDefault();
  $(this).addClass('btn-spinner');
})

function costPrice() {
  var costArray = [];

  $('.costContent p').each(function() {
    var costPrice = $(this).text().substring(1).replace(/,/g, '');
    costArray.push(costPrice);
  });
  
  var maxCost = Math.max.apply(Math, costArray);
  
  var maxCostIndex = costArray.indexOf(maxCost.toString());
  
  var maxWidth = $('.cost').eq(maxCostIndex).find('.costColumn').width();
  
  $('.cost').each(function() {
    var i = $(this).index();
    var width = costArray[i]/maxCost*maxWidth;
    $(this).find('.costColumn').css('width', width + 'px').addClass('costColumn_Shorter');
  })
} 

if ($('.cost').length) {
  costPrice();
}

if ($(window).width() > 1199 && $('.costsContent').length) {
  const psCosts = new PerfectScrollbar('.costsContent');
}






 $(document).on('change', '.file input', function () {
   var fileName = $(this).val().replace(/C:\\fakepath\\/i, '');
   $('.file__content-title').html(fileName);
   $('.file__field').hide();
   console.log(this.files[0].size);
   $('.file__content').toggleClass('file__content--active');
 });


$(document).ready(function () {
  $('.js-submit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var form_data = form.serialize();
    form.trigger('reset');
    $.ajax({
      type: 'POST',
      url: './send.php',
      data: form_data,
      success: function (data) {
        $('.form__content').hide();
        $('.form__success').fadeIn();
      }
    });
  });
});

$('.form__password-view').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('form__password-view--active');
  var inputType = $(this).closest('.form__item-password').find('.input');
  if (inputType.attr('type') == 'password') {
    inputType.attr('type', 'text');
  }
  else {
    inputType.attr('type', 'password');
  }
})








$('.integrations__btn').click(function(e) {
  e.preventDefault();
  $(this).hide();
  $('.integrations__item').fadeIn().css('display', 'flex');
});



$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').toggleClass('menu__content--active');
  $('.header').toggleClass('header--menu');
  $('body').toggleClass('fixed');
})
Fancybox.bind('[data-fancybox]', {
  autoFocus: false,
  dragToClose: false
});

$('.modal__close').click(function(e) {
  e.preventDefault();
  Fancybox.close();
})



$('.notificationBtn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('notificationBtn_Less');
  $(this).closest('p').find('.notificationText').toggleClass('notificationText_Open')
});

$('.notification').each(function() {
  var text = $(this).find('.notificationText');
  var btn = $(this).find('.notificationBtn');

  if (text.prop('scrollWidth') > text.width() + 1 ) {
    btn.addClass('notificationBtn_Active');
  }
})








if ($('.popupContent').length) {
  const psPopup = new PerfectScrollbar('.popupContent');
}




if ($('.select__input').length) {

  $('.select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      closeOnSelect: false,
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    });

    $(this).on('change', function () {
      let select = $(this)
      if ($(this).val().length > 2) {

        $(this).next('span.select2').find('ul').html(function () {
          let count = select.select2('data').length - 1;
          return "<li>Выбрано " + count + "</li>";
        })
      }
    });
  })

  $('.select_Countries .select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      templateResult: formatState,
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    }).on('select2:select', function (e) {
      var data = e.params.data;
      $('.select_Countries .select2-selection__rendered').prepend('<img src="./img/' + data.id + '.svg" class="select2-selection__rendered-icon">')
    });
    
    var countryInitial = $('.select_Countries .select__input').val();
    $('.select_Countries .select2-selection__rendered').prepend('<img src="./img/' + countryInitial + '.svg" class="select2-selection__rendered-icon">');
  });
  
  function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    var baseUrl = "./img";
    var $state = $(
      '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.svg" /> ' + state.text + '</span>'
    );
    return $state;
  };

  $('.select--normal .select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      placeholder: "Select",
      dropdownParent: container
    }).on('select2:open', function (e) {
      setTimeout(function () {
        const ps = new PerfectScrollbar('.select2-results__options');
      }, 100)
    })
  });
};


$('.jsStageNext').click(function(e) {
  e.preventDefault();
  var activeIndex = $('.stagesItem_Active').index();
  $('.stagesItem').removeClass('stagesItem_Active');
  $('.stagesItem').eq(activeIndex+1).addClass('stagesItem_Active');
  $('.stagesHeaderItem').eq(activeIndex+1).addClass('stagesHeaderItem_Active');
})


$('.statusesSectionItem').each(function() {
  var weight = $(this).text();
  var weightMax = $(this).closest('.statusesSection').find('.statusesSectionOverall').text();
  if (weight) {
    $(this).css('flex', '0 0 ' + weight/weightMax*100 + '%');
  }
});

function statusesPush() {
  var statusesOverall = [];

  $('.statusesSectionOverall').each(function() {
    statusesOverall.push($(this).text());
  });
  
  var minOverall = Math.min.apply(Math, statusesOverall).toString();
  
  var maxOverall = Math.max.apply(Math, statusesOverall).toString();

  var minOverallIndex = statusesOverall.indexOf(minOverall);

  var maxOverallIndex = statusesOverall.indexOf(maxOverall);
  
  var lgSectionHeight = $('.statusesSection').eq(maxOverallIndex).find('.statusesSectionColumn').height();
  
  var smSectionHeight = minOverall/maxOverall;

  $('.statusesSection').eq(minOverallIndex).find('.statusesSectionColumn').css('flex', '0 0 ' + smSectionHeight*100 + '%');
}

if ($('.statusesSection').length) {
  statusesPush();
}


$('.table__item--title').click(function(e) {
  e.preventDefault();
  $(this).closest('.table__row').find('.table__row-content').slideToggle();
});

$('.table__more').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('table__more--active');
  $(this).closest('.table__row').find('.table__row-content').slideToggle();
})




$('.js-visible-btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('visible_Active');
  $(this).closest('.js-visible-wrapper').find('.js-visible-content').toggleClass('js-visible-content--hidden');
//  $(this).closest('.js-visible-wrapper').find('.js-visible-content').each(function() {
//    $(this).attr('data-content', $(this).text());
//  }).text('********');
});

